import os
import webbrowser

valid_tags = {'fisk', 'kött', 'kyckling', 'japanskt', 'kinesiskt', 'arabiskt', 'vegetariskt', 'friterat', 'vietnamesiskt', 'efterrätter', 'kakor',
              'soppor', 'mjuka kakor', 'matbröd', 'godis', 'husmanskost'}

valid_ingredients = {'mjöl', 'lök', 'mjölk', 'ägg', 'peppar', 'salt', 'isbergssallad', 'spenat', 'tomat'}

def add_valid_ingredient(ingredient_str, recipe_manager):
    global valid_ingredients
    ingredient_str = ingredient_str.lower()
    valid_ingredients.add(ingredient_str)
    recipe_manager.save_valid_sets()

def add_valid_tag(tag_str, recipe_manager):
    global valid_tags
    tag_str = tag_str.lower()
    valid_tags.add(tag_str)
    recipe_manager.save_valid_sets()

def remove_valid_tag(tag_str, recipe_manager):
    global valid_tags
    tag_str = tag_str.lower()
    if tag_str in valid_tags:
        valid_tags.remove(tag_str)
        recipe_manager.save_valid_sets()

def remove_valid_ingredient(ingredient_str, recipe_manager):
    global valid_ingredients
    ingredient_str = ingredient_str.lower()
    if ingredient_str in valid_ingredients:
        valid_ingredients.remove(ingredient_str)
        recipe_manager.save_valid_sets()

def get_valid_ingredients():
    return valid_ingredients

def get_valid_tags():
    return valid_tags




class RecipeObject():
    def __init__(self, recipe_manager, file_name, name=None, image=None, tags=None, ingredients=None, rating=None, recipe_comment=None):
        # The current state of the save and load funcions can cause parameters to be None
        # Variable path can be the absoute- or relative path to the .htm(l) file
        if name is not None:
            self.__name = name
        else:
            self.__name = "No Name"

        if image is not None:
            self.__image = image
        else:
            self.__image = None

        if tags is not None:
            self.__tags = tags
        else:
            self.__tags = set()

        if ingredients is not None:
            self.__ingredients = ingredients
        else:
            self.__ingredients = set()

        if rating is not None:
            self.__rating = rating
        else:
            self.__rating = int(1)

        if recipe_comment is not None:
            self.__recipe_comment = recipe_comment
        else:
            self.__recipe_comment = str()

        if not file_name: # if path changes during run-time, the old path must be removed from reciManager which have a separate set with all the paths
            raise Exception("the file_name argument is not optional")
        self.__file_name = None

        if(os.path.isfile(f"{recipe_manager.recipe_root}\\{file_name}")):
            if(file_name.endswith(".html") or file_name.endswith(".htm") or file_name.endswith(".pdf")):
                self.__file_name = file_name
            else:
                # print(f"Not HTML, entered path: {path}")
                pass
        else:
            # print("enteted path isn't a file")
            pass

    def set_name(self, name, recipe_manager):
        if(name not in recipe_manager.recipes):
            try:
                os.remove(f"{recipe_manager.save_root}\\{self.__name}.dat")
                recipe_manager.recipes.pop(self.__name)
                self.__name = name
                recipe_manager.recipes[self.__name] = self
                recipe_manager.save_recipe(self)
            except OSError:
                raise Exception("Something went wrong in set_name")
                exit(-1)




    def set_rating(self, rating, recipe_manager):
        if isinstance(rating, int):
            self.__rating = rating
            recipe_manager.update_recipe(self)
        else:
            raise Exception(f"rating must be an int but was {type(rating)}")

    def set_comment(self, comment, recipe_manager):
        if isinstance(comment, str):
            self.__recipe_comment = comment
            recipe_manager.update_recipe(self)
        else:
            raise Exception(f"comment must be an string but was {type(comment)}")

    def get_ingredients(self):
        return self.__ingredients

    def get_name(self):
        return self.__name

    def get_path(self, recipe_manager):
        return f"{recipe_manager.recipe_root}\\{self.__file_name}"

    def get_file_name(self):
        return self.__file_name

    def get_tags(self):
        return self.__tags

    def get_image(self):
        return self.__image

    def get_rating(self):
        return self.__rating

    def get_comment(self):
        return self.__recipe_comment

    def open(self, recipe_manager):
        path = self.get_path(recipe_manager)
        absolute_path = os.path.abspath(path)
        webbrowser.open_new_tab(absolute_path)

    def add_tag(self, tag, recipe_manager):
        if tag in valid_tags:
            self.__tags.add(tag)
            recipe_manager.update_recipe(self)
        else:
            raise Exception(f"The tag \"{tag}\" is not valid. Attempted to add the tag to \"{self.__name}\"")

    def add_ingredient(self, ingredient, recipe_manager): # NOT TESTED
        if ingredient in valid_ingredients:
            self.__ingredients.add(ingredient)
            recipe_manager.update_recipe(self)

    def remove_tag(self, tag, recipe_manager):
        if tag in self.__tags:
            self.__tags.remove(tag)
            recipe_manager.update_recipe(self)

    def remove_ingredient(self, ingredient, recipe_manager):
        if ingredient in self.__ingredients:
            self.__ingredients.remove(ingredient)
            recipe_manager.update_recipe(self)