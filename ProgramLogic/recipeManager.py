import sys
import os
import pickle
import traceback
import ProgramLogic.recipe as recipe
from ProgramLogic.recipe import RecipeObject
import os
import configparser


class RecipeManager():
    def __init__(self):
        self.save_root = None
        self.recipe_root = None
        self.recipes = dict()
        self.__file_names = set()  # Filenames to each recepies that currently are in the dict "recipes"

        # Setup configuration
        if not os.path.exists("conf"):
            print("Could not find a conf folder, creating folder in root directory")
            os.mkdir("conf")
        if not os.path.isfile("conf/config.cfg"):
            with open("conf/config.cfg", "w", encoding='utf-8') as file:
                file.write("""[PATH]
html_recipe_path = ...
save_file_path = ...""")
            print('config.cfg created in conf dicectory. Replace "..." with your path(s)')
            sys.exit(0)

        # read configuration
        config_parser = configparser.ConfigParser(comment_prefixes='/', allow_no_value=True)
        config_parser.read('conf/config.cfg')

        try:
            self.save_root = config_parser["PATH"]["save_file_path"]
            self.recipe_root = config_parser["PATH"]["html_recipe_path"]
        except:
            raise Exception("configparser error")
            sys.exit(-1)

        # Create a save-folder if non exists
        try:
            if (not os.path.exists(self.save_root)):
                print("Could not find a save folder, creating folder")
                os.mkdir(self.save_root)
                os.mkdir(f"{self.save_root}\\programData")

        except:
            traceback.print_exc()
            sys.exit()

        self.load_valid_sets()
        self.load_all_recipes()
        self.find_new_recipes()

    def save_recipe(self, recipe_object):
        save_data = dict()
        try:
            save_data["name"] = recipe_object.get_name()
            save_data["image"] = recipe_object.get_image()
            save_data["tags"] = recipe_object.get_tags()
            save_data["file_name"] = recipe_object.get_file_name()
            save_data["ingredients"] = recipe_object.get_ingredients()
            save_data["rating"] = recipe_object.get_rating()
            save_data["recipe_comment"] = recipe_object.get_comment()

            pickle.dump(save_data, open(f"{self.save_root}\\{recipe_object.get_name()}.dat", "wb"))
        except:
            print("Something went wrong during saving")
            traceback.print_exc()

    def save_all_recipes(self):
        for recipe in self.recipes.values():
            self.save_recipe(recipe)

    def load_recipe(self, recipe_name):
        # Check recipes paths(method in recipe does that)
        try:
            loaded_data = pickle.load(open(f"{self.save_root}\\{recipe_name}", 'rb'))
            name = loaded_data.get("name")
            image = loaded_data.get("image")
            tags = loaded_data.get("tags")
            file_name = loaded_data.get("file_name")
            ingredients = loaded_data.get("ingredients")
            rating = loaded_data.get("rating")
            recipe_comment = loaded_data.get("recipe_comment")
            self.recipes[name] = RecipeObject(self, file_name, name, image, tags, ingredients, rating, recipe_comment)
            self.__file_names.add(file_name)

        except:
            print("Error when loading saves")
            traceback.print_exc()

    def load_all_recipes(self):
        """Populate recipe dict with all saved recipes"""
        try:
            for file in os.listdir(self.save_root):
                if os.path.isdir(f"{self.save_root}\\{file}"):
                    #if this is true, it's a folder, not a file
                    continue
                self.load_recipe(file)
        except:
            raise Exception("Something went wrong when loading all recipes")
            traceback.print_exc()

    def find_new_recipes(self):  # works
        """Populate list with recipes found in the recipe-directory that
        aren't already in the list"""
        for recipe_name in os.listdir(self.recipe_root):  # Loop over all files in recipe-directory
            recipe_file_name = f"{recipe_name}"
            if recipe_file_name.endswith(".html") or recipe_file_name.endswith(".htm") or recipe_file_name.endswith(".pdf"):
                if recipe_file_name not in self.__file_names:
                    self.recipes[recipe_name] = RecipeObject(self, recipe_file_name, recipe_name)
                    self.__file_names.add(recipe_file_name)

    def update_recipe(self, recipe_object):
        if recipe_object in self.recipes.values():
            self.save_recipe(recipe_object)
        else:
            print("The recipe you tried to update doesn't exist")

    def save_valid_sets(self):
        pickle.dump(recipe.valid_tags, open(f"{self.save_root}\\programData\\tags.dat", "wb"))
        pickle.dump(recipe.valid_ingredients, open(f"{self.save_root}\\programData\\ingredients.dat", "wb"))

    def load_valid_sets(self):
        try:
            recipe.valid_ingredients = pickle.load(open(f"{self.save_root}\\programData\\ingredients.dat", "rb"))
            recipe.valid_tags = pickle.load(open(f"{self.save_root}\\programData\\tags.dat", "rb"))
        except:
            pass

    def return_list_filtered_by_name(self, name_string):
        """All recipes containing the worlds in name_string
        will be returned"""
        sorted_list = list()
        name_string = name_string.lower()
        for recipe in self.recipes.values():
            if name_string in recipe.get_name().lower():
                sorted_list.append(recipe)
        return sorted_list

    def return_list_filtered_by_tag(self, tag_set):
        """Returns a list where only recipes
        with mentioned tags will be returned"""
        sorted_list = list()
        for recipe in self.recipes.values():
            current_recipe_tags = recipe.get_tags()
            if(tag_set.issubset(current_recipe_tags)):
                sorted_list.append(recipe)
        return sorted_list

    def return_list_filtered_by_ingredients(self, ingredient_set):
        """Returns a list where only recipes
        with mentioned ingredients will be returned"""
        sorted_list = list()
        for recipe in self.recipes.values():
            current_recipe_ingredients = recipe.get_ingredients()
            if ingredient_set.issubset(current_recipe_ingredients):
                sorted_list.append(recipe)
        return sorted_list
