import sys
import os
vendor_win_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "vendor_win")
sys.path.append(vendor_win_path)
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5 import QtWidgets, QtCore
import populator
from UI_design import recepie_info, recipe_search, recepie_edit, text_input_dialog

if __name__ == "__main__":
    app = QApplication(sys.argv)

    window_main = QMainWindow()
    window_info = QMainWindow()
    window_info.move(0, 0)
    window_edit = QMainWindow()
    window_edit.setWindowFlag(QtCore.Qt.WindowCloseButtonHint, False)

    # set up ui
    ui_search = recipe_search.Ui_MainWindow()
    ui_search.setupUi(window_main)
    ui_info = recepie_info.Ui_MainWindow()
    ui_info.setupUi(window_info)
    ui_edit = recepie_edit.Ui_MainWindow()
    ui_edit.setupUi(window_edit)
    win_dialog = QtWidgets.QDialog()
    ui_dialog = text_input_dialog.Ui_Dialog()
    ui_dialog.setupUi(win_dialog)

    # set variables in populator
    populator.win_main = window_main
    populator.win_info = window_info
    populator.win_edit = window_edit
    populator.win_dialog = win_dialog
    populator.ui_search = ui_search
    populator.ui_info = ui_info
    populator.ui_edit = ui_edit
    populator.ui_dialog = ui_dialog
    populator.init_search_window()
    window_main.show()

    sys.exit(app.exec())
