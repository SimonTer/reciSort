import os.path
from PyQt5 import QtWidgets, QtCore
from PyQt5.Qt import Qt
from ProgramLogic.recipeManager import RecipeManager
import traceback
from UI_design import recepie_info, recipe_search, recepie_edit, text_input_dialog
from ProgramLogic.recipe import add_valid_ingredient, add_valid_tag, remove_valid_ingredient, \
    remove_valid_tag, get_valid_tags, get_valid_ingredients

win_main = None
win_info = None
win_edit = None
win_dialog = None
ui_search = None
ui_info = None
ui_edit = None
ui_dialog = None
recipe_manager = RecipeManager()
selected_tags = set()
selected_ingredients = set()
edited_selected_tags = set()
edited_selected_ingredients = set()
inte = 0

def init_search_window():
    search_window_populate_recipe_view()

    # populate the tag listWidget in search window
    ui_search.listWidget_Tags.clear()
    populate_listWidget(ui_search.listWidget_Tags, get_valid_tags(), True, selected_tags)

    # populate the ingredient listWidget in search window
    ui_search.listWidget_Ingredients.clear()
    populate_listWidget(ui_search.listWidget_Ingredients, get_valid_ingredients(), True, selected_ingredients)

    ui_search.listWidget_Tags.itemChanged.connect(register_valid_item)
    ui_search.listWidget_Ingredients.itemChanged.connect(register_valid_item)
    ui_search.lineEdit_Search_Recipe_Name.textChanged.connect(search_window_sort_recipes)
    ui_search.spinBox_Rating.valueChanged.connect(search_window_sort_recipes)
    ui_search.lineEdit_Search_Tag.textEdited.connect(search_window_search_valid_tag)
    ui_search.lineEdit_Search_Ingredient.textEdited.connect(search_window_search_valid_ingredient)
    ui_search.listWidget_Recipes.itemClicked.connect(info_window_update_gui)
    ui_info.lineEdit_Search_Tag.textEdited.connect(info_window_search_for_valid_tag)
    ui_info.lineEdit_Search_Ingredient.textEdited.connect(info_window_search_for_valid_ingredient)
    ui_info.pushButton_Open.clicked.connect(info_window_open_recipe)
    ui_info.pushButton_Edit.clicked.connect(edit_window_update_gui)
    ui_edit.lineEdit_Search_Ingredient.textEdited.connect(edit_window_search_for_valid_ingredients)
    ui_edit.lineEdit_Search_Tag.textEdited.connect(edit_window_search_for_valid_tags)
    ui_edit.pushButton_Save.clicked.connect(edit_window_save_recipe_and_close)
    ui_edit.listWidget_Ingredients.itemChanged.connect(register_edited_valid_item)
    ui_edit.listWidget_Tags.itemChanged.connect(register_edited_valid_item)
    ui_edit.pushButton_Cancel.clicked.connect(edit_window_cancel)
    ui_edit.pushButton_New_Tag.clicked.connect(new_tag)
    ui_edit.pushButton_Remove_Tag.clicked.connect(remove_tag)
    ui_edit.pushButton_New_Ingredient.clicked.connect(new_ingredient)
    ui_edit.pushButton_Remove_Ingredient.clicked.connect(remove_ingredient)

    disable_unavailable_sort_options()


def disable_unavailable_sort_options():
    try:

        # get name of all recipes shown in gui
        resipe_names = [ui_search.listWidget_Recipes.item(index).text() for index in range(ui_search.listWidget_Recipes.count())]

        # find all valids that can be combineed with currently selected valids
        enabled_tags = set()
        enabled_ingredients = set()
        for name in resipe_names:
            recipe = recipe_manager.recipes.get(name)
            # get tags
            recipe_tags = recipe.get_tags()
            recipe_ingredients = recipe.get_ingredients()

            if selected_tags.issubset(recipe_tags):
                enabled_tags.update(recipe.get_tags())
            # get ingredients
            if selected_ingredients.issubset(recipe_ingredients):
                enabled_ingredients.update(recipe.get_ingredients())

        # disable/enable tags
        tag_list = [ui_search.listWidget_Tags.item(index) for index in range(ui_search.listWidget_Tags.count())]
        for tag in tag_list:
            if tag.text() in enabled_tags:
                # enable valid
                tag.setFlags(QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsUserCheckable)
            else:
                # disable valid
                tag.setFlags(QtCore.Qt.ItemIsSelectable)
                tag.setCheckState(0)
                # place items at the bottom
                remove_index = ui_search.listWidget_Tags.row(tag)
                temp_item = ui_search.listWidget_Tags.takeItem(remove_index)
                ui_search.listWidget_Tags.addItem(temp_item)

            # disable/enable ingredients
            ingredient_list = [ui_search.listWidget_Ingredients.item(index) for index in range(ui_search.listWidget_Ingredients.count())]
            for ingredient in ingredient_list:
                if ingredient.text() in enabled_ingredients:
                    # enable valid
                    ingredient.setFlags(QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsUserCheckable)
                else:
                    # disable valid
                    ingredient.setFlags(QtCore.Qt.ItemIsSelectable)
                    ingredient.setCheckState(0)
                    # place items at the bottom
                    remove_index = ui_search.listWidget_Ingredients.row(ingredient)
                    temp_item = ui_search.listWidget_Ingredients.takeItem(remove_index)
                    ui_search.listWidget_Ingredients.addItem(temp_item)

    except:
        traceback.print_exc()

def search_window_search_valid_ingredient():
    search_valid_lists(get_valid_ingredients(), ui_search.lineEdit_Search_Ingredient.text(), ui_search.listWidget_Ingredients, True, selected_ingredients)
    disable_unavailable_sort_options()

def search_window_search_valid_tag():
    search_valid_lists(get_valid_tags(), ui_search.lineEdit_Search_Tag.text(), ui_search.listWidget_Tags, True, selected_tags)
    disable_unavailable_sort_options()

def search_window_populate_recipe_view(recipe_list=None):
    if recipe_list is None:
        recipe_list = [reci.get_name() for reci in recipe_manager.recipes.values() if reci.get_rating() >= ui_search.spinBox_Rating.value()]
    else:
        recipe_list = [reci.get_name() for reci in recipe_list if reci.get_rating() >= ui_search.spinBox_Rating.value()]

    populate_listWidget(ui_search.listWidget_Recipes, recipe_list, False)
    disable_unavailable_sort_options()

def search_window_sort_recipes():
    try:
        searched_name = ui_search.lineEdit_Search_Recipe_Name.text()

        if len(selected_tags) + len(selected_ingredients) + len(searched_name) == 0:
            search_window_populate_recipe_view()
            return

        # if no specific value is chosen, all recipes are valid values
        if len(selected_ingredients) == 0:
            recipes_with_matching_ingredients = recipe_manager.recipes.values()
        else:
            recipes_with_matching_ingredients = recipe_manager.return_list_filtered_by_ingredients(selected_ingredients)

        if len(selected_tags) == 0:
            recipes_with_matching_tags = recipe_manager.recipes.values()
        else:
            recipes_with_matching_tags = recipe_manager.return_list_filtered_by_tag((selected_tags))

        if len(searched_name) == 0:
            recipes_with_matching_name = recipe_manager.recipes.values()
        else:
            recipes_with_matching_name = recipe_manager.return_list_filtered_by_name(searched_name)

        sorted_recipes = set(recipes_with_matching_ingredients).intersection(
            set(recipes_with_matching_tags).intersection(set(recipes_with_matching_name)))
        sorted_recipes = list(sorted_recipes)

        search_window_populate_recipe_view(sorted_recipes)
    except:
        traceback.print_exc()

def register_valid_item(item):
    # Toggle items in in selected-list. If an item is checked or unchecked,
    # this method will add or remove the selected item from the list. After the
    # item is added/removed it will search and update the recipe window
    from_listWidget = item.listWidget()  # get from which list the item is from

    # select what list to use depending on what list the item is from
    if from_listWidget == ui_search.listWidget_Tags:
        selected_list = selected_tags
    elif from_listWidget == ui_search.listWidget_Ingredients:
        selected_list = selected_ingredients
    # add or remove item from list depending on if it already is in the list or not
    if item.checkState() == 0 and item.text() in selected_list:
        selected_list.remove(item.text())
        search_window_sort_recipes()
    elif item.checkState() == 2 and item.text() not in selected_list:
        selected_list.add(item.text())
        search_window_sort_recipes()



def populate_listWidget(listWidget, item_text_list, have_checkboxes_bool, selected_valid_set=None):
    if have_checkboxes_bool:
        if selected_valid_set is None:
            raise Exception("If have_checkboxes_bool is true, selected_valid_set must be assigned a selected_set")
            exit(-1)

    listWidget.clear()
    # populate list, add checkboxes if have_checkboxes is true
    for valid_data in sorted(item_text_list):
        temp_item = QtWidgets.QListWidgetItem(valid_data)
        if have_checkboxes_bool:
            if valid_data in selected_valid_set:
                temp_item.setCheckState(2)
            else:
                temp_item.setCheckState(0)
        listWidget.addItem(temp_item)

def search_valid_lists(valid_set, search_phrace_string, listWidget, have_checkboxes_bool, selected_valid_set=None):
    # check if search phrase is empty, if empty populate list with all items in valid set
    if len(search_phrace_string) == 0:
        populate_listWidget(listWidget, valid_set, have_checkboxes_bool, selected_valid_set)
        return

    # search for tag/ingredient
    search_phrace_string = search_phrace_string.lower()
    matching_result = list()
    for valid_data in valid_set:
        if search_phrace_string in valid_data:
            matching_result.append(valid_data)

    #populate list
    populate_listWidget(listWidget, matching_result, have_checkboxes_bool, selected_valid_set)

def info_window_open_recipe():
    recipe_manager.recipes.get(ui_info.label_Recipe_Name.text()).open(recipe_manager)

def info_window_update_gui(event):
    if not win_info.isVisible():
        win_info.show()
    recipe = recipe_manager.recipes.get(event.text())
    # print(recipe)
    # print(recipe.get_path())
    # print(recipe.get_name())
    ui_info.label_Recipe_Name.setText(recipe.get_name())
    ui_info.textBrowser_Comment.setText(recipe.get_comment())
    ui_info.label_Rating.setText(f"Betyg: {recipe.get_rating()}")
    ui_info.label_Path.setText(f"Path: {os.path.abspath(recipe.get_path(recipe_manager))}")
    # populate tags and ingredients
    populate_listWidget(ui_info.listWidget_Tags, recipe.get_tags(), False)
    populate_listWidget(ui_info.listWidget_Ingredients, recipe.get_ingredients(), False)


def info_window_search_for_valid_tag(event):
    recipe_tags = recipe_manager.recipes.get(ui_info.label_Recipe_Name.text()).get_tags()
    search_valid_lists(recipe_tags, event, ui_info.listWidget_Tags, False)

def info_window_search_for_valid_ingredient(event):
    recipe_ingredients = recipe_manager.recipes.get(ui_info.label_Recipe_Name.text()).get_ingredients()
    search_valid_lists(recipe_ingredients, event, ui_info.listWidget_Ingredients, False)

def edit_window_update_gui():
    global edited_selected_tags
    global edited_selected_ingredients
    win_main.setDisabled(True)
    win_info.setDisabled(True)
    if not win_edit.isVisible():
        win_edit.show()
    recipe_name = ui_info.label_Recipe_Name.text()
    recipe = recipe_manager.recipes.get(recipe_name)
    ui_edit.spinBox_Betyg.setValue(recipe.get_rating())
    ui_edit.lineEdit_Recipe_Name.setText(recipe_name)
    ui_edit.textEdit_Comment.setText(recipe.get_comment())

    edited_selected_ingredients.clear()
    edited_selected_tags.clear()
    edited_selected_tags = recipe.get_tags().copy()
    edited_selected_ingredients = recipe.get_ingredients().copy()

    # populate tags and ingredients
    populate_listWidget(ui_edit.listWidget_Tags, get_valid_tags(), True, edited_selected_tags)
    populate_listWidget(ui_edit.listWidget_Ingredients, get_valid_ingredients(), True, edited_selected_ingredients)

def register_edited_valid_item(item):
    global edited_selected_tags
    global edited_selected_ingredients
    from_listWidget = item.listWidget()
    # variables for updating search result list
    tempTextList = None
    valid_set = None

    if from_listWidget == ui_edit.listWidget_Ingredients:
        selected_list = edited_selected_ingredients
        tempTextList = get_valid_ingredients()
        valid_set = edited_selected_ingredients
        ui_edit.lineEdit_Search_Ingredient.setText("")
    elif from_listWidget == ui_edit.listWidget_Tags:
        selected_list = edited_selected_tags
        tempTextList = get_valid_tags()
        valid_set = edited_selected_tags
        ui_edit.lineEdit_Search_Tag.setText("")

    if item.checkState() == 0 and item.text() in selected_list:
        selected_list.remove(item.text())
    elif item.checkState() == 2 and item.text() not in selected_list:
        selected_list.add(item.text())

    # after the item is selected,
    # the search text will be set to emptied and the list will be repopulated
    populate_listWidget(from_listWidget, tempTextList, True, valid_set)


def edit_window_search_for_valid_tags(event):
    search_valid_lists(get_valid_tags(), event, ui_edit.listWidget_Tags, True, edited_selected_tags)

def edit_window_search_for_valid_ingredients(event):
        search_valid_lists(get_valid_ingredients(), event, ui_edit.listWidget_Ingredients, True, edited_selected_ingredients)

def edit_window_save_recipe_and_close(event):

    recipe = recipe_manager.recipes.get(ui_info.label_Recipe_Name.text())


    # Remove all tags/ingredients that have been deselected
    for current_tags in list(recipe.get_tags()):
        if current_tags not in edited_selected_tags:
            recipe.remove_tag(current_tags, recipe_manager)
    for current_ingredients in list(recipe.get_ingredients()):
        if current_ingredients not in edited_selected_ingredients:
            recipe.remove_ingredient(current_ingredients, recipe_manager)

    # save tags
    for tag in edited_selected_tags:
        recipe.add_tag(tag, recipe_manager)
    for ingredient in edited_selected_ingredients:
        recipe.add_ingredient(ingredient, recipe_manager)

    recipe.set_comment(ui_edit.textEdit_Comment.toPlainText() , recipe_manager)
    recipe.set_rating(ui_edit.spinBox_Betyg.value(), recipe_manager)
    recipe.set_name(ui_edit.lineEdit_Recipe_Name.text(), recipe_manager)
    win_edit.setVisible(False)

    info_window_update_gui(QtWidgets.QListWidgetItem(recipe.get_name()))
    search_window_sort_recipes()

    ui_edit.lineEdit_Search_Tag.setText("")
    ui_edit.lineEdit_Search_Ingredient.setText("")
    win_main.setDisabled(False)
    win_info.setDisabled(False)

def edit_window_cancel(event):
    ui_edit.lineEdit_Search_Tag.setText("")
    ui_edit.lineEdit_Search_Ingredient.setText("")
    win_edit.setVisible(False)
    win_main.setDisabled(False)
    win_info.setDisabled(False)

def close_dialog():
    ui_dialog.pushButton_Ok.disconnect()
    ui_dialog.pushButton_Cancel.disconnect()
    ui_dialog.lineEdit_Input.setText("")
    win_edit.setDisabled(False)
    win_dialog.setVisible(False)

def open_dialog(textHint):
    win_edit.setDisabled(True)
    ui_dialog.label.setText(textHint)
    win_dialog.show()

def new_tag():
    open_dialog("Enter new tag")

    def ok():
        add_valid_tag(ui_dialog.lineEdit_Input.text(), recipe_manager)

        # update gui
        edit_window_search_for_valid_tags(ui_edit.lineEdit_Search_Tag.text())
        info_window_search_for_valid_tag(ui_info.lineEdit_Search_Tag.text())
        search_window_search_valid_tag()

        close_dialog()
    def cancel():
        close_dialog()

    ui_dialog.pushButton_Ok.clicked.connect(ok)
    ui_dialog.pushButton_Cancel.clicked.connect(cancel)

def remove_tag(textHint):
    open_dialog("Enter tag to remove")

    def ok():
        tag_name = ui_dialog.lineEdit_Input.text()

        # remove tag from selected list if they have been selected
        if tag_name in edited_selected_tags:
            edited_selected_tags.remove(tag_name)
        if tag_name in selected_tags:
            selected_tags.remove(tag_name)

        # remove tag from recipes
        for recipe in recipe_manager.recipes.values():
            if tag_name in recipe.get_tags():
                recipe.remove_tag(tag_name, recipe_manager)

        # remove tag from valid tags
        remove_valid_tag(tag_name, recipe_manager)

        # update gui
        edit_window_search_for_valid_tags(ui_edit.lineEdit_Search_Tag.text())
        info_window_search_for_valid_tag(ui_info.lineEdit_Search_Tag.text())
        search_window_search_valid_tag()

        close_dialog()
    def cancel():
        close_dialog()

    ui_dialog.pushButton_Ok.clicked.connect(ok)
    ui_dialog.pushButton_Cancel.clicked.connect(cancel)

def new_ingredient():
    open_dialog("Enter new ingredient")

    def ok():
        add_valid_ingredient(ui_dialog.lineEdit_Input.text(), recipe_manager)

        # update gui
        edit_window_search_for_valid_ingredients(ui_edit.lineEdit_Search_Ingredient.text())
        info_window_search_for_valid_ingredient(ui_info.lineEdit_Search_Ingredient.text())
        search_window_search_valid_tag()

        close_dialog()

    def cancel():
        close_dialog()

    ui_dialog.pushButton_Ok.clicked.connect(ok)
    ui_dialog.pushButton_Cancel.clicked.connect(cancel)

def remove_ingredient():
    open_dialog("Enter ingredient to remove")

    def ok():
        ingredient_name = ui_dialog.lineEdit_Input.text()

        # remove ingredient from selected list if they have been selected
        if ingredient_name in edited_selected_ingredients:
            edited_selected_ingredients.remove(ingredient_name)
        if ingredient_name in selected_ingredients:
            selected_ingredients.remove(ingredient_name)

        # remove tag from recipes
        for recipe in recipe_manager.recipes.values():
            if ingredient_name in recipe.get_ingredients():
                recipe.remove_ingredient(ingredient_name, recipe_manager)

        # remove tag from valid tags
        remove_valid_ingredient(ingredient_name, recipe_manager)

        # update gui
        edit_window_search_for_valid_ingredients(ui_edit.lineEdit_Search_Ingredient.text())
        info_window_search_for_valid_ingredient(ui_info.lineEdit_Search_Ingredient.text())
        search_window_search_valid_tag()

        close_dialog()


    def cancel():
        close_dialog()

    ui_dialog.pushButton_Ok.clicked.connect(ok)
    ui_dialog.pushButton_Cancel.clicked.connect(cancel)